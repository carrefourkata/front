import { Component, Inject, OnInit } from '@angular/core';
import { OKTA_AUTH, OktaAuthStateService } from '@okta/okta-angular';
import OktaAuth from '@okta/okta-auth-js';
declare const SwaggerUIBundle: any;

@Component({
  selector: 'app-swagger-ui',
  templateUrl: './swagger-ui.component.html',
  styleUrls: ['./swagger-ui.component.scss'],
  standalone: true})
export class SwaggerUIComponent implements OnInit {

  constructor(public authStateService: OktaAuthStateService, @Inject(OKTA_AUTH) private oktaAuth: OktaAuth) {}

  ngOnInit(): void {

    const token = this.oktaAuth.getAccessToken();
    SwaggerUIBundle({
      dom_id: '#swagger-ui',
      layout: 'BaseLayout',
      presets: [
        SwaggerUIBundle.presets.apis,
        SwaggerUIBundle.SwaggerUIStandalonePreset
      ],
      url: '../assets/swagger.json',
      
      docExpansion: 'none',
      operationsSorter: 'alpha',
      requestInterceptor: (request: { headers: { Authorization: string; }; }) => {
        // Add the JWT token to the request headers
        if (token) {
          request.headers.Authorization = `Bearer ${token}`;
        }
        return request;
      }
    });
  }
}