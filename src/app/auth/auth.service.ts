import { Inject, Injectable } from '@angular/core';
import { OktaAuthStateService, OKTA_AUTH  } from '@okta/okta-angular';
import OktaAuth from '@okta/okta-auth-js';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(public authStateService: OktaAuthStateService, @Inject(OKTA_AUTH) private oktaAuth: OktaAuth) {

  }

  async login() {
    await this.oktaAuth.signInWithRedirect({ originalUri: '/' });
  }

  async logout() {
    await this.oktaAuth.signOut();
  }
}