import { ApplicationConfig, Injector, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { OktaAuthModule } from '@okta/okta-angular';
import OktaAuth from '@okta/okta-auth-js';
import authConfig from './auth/auth.config';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { authInterceptor } from './auth/auth.interceptor';

export const appConfig: ApplicationConfig = {
   
  providers: [ 
    importProvidersFrom(
      OktaAuthModule.forRoot({
        oktaAuth: new OktaAuth(authConfig.oidc)
      })),
  provideRouter(routes), 
  provideHttpClient(withInterceptors([
    authInterceptor
  ]))]
};
