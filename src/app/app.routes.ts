import { Routes } from '@angular/router';
import { OktaAuthGuard, OktaCallbackComponent } from '@okta/okta-angular';
import { HomeComponent } from './home/home.component';
import { SwaggerUIComponent } from './swagger-ui/swagger-ui.component';

export const routes: Routes = [
    { path: 'swagger', component: SwaggerUIComponent, canActivate: [OktaAuthGuard] },
    { path: 'home', component: HomeComponent, canActivate: [OktaAuthGuard] },
    { path: 'login/callback', component: OktaCallbackComponent }
];
