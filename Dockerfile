### STAGE 1: Build ###
FROM node:lts-alpine AS build
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install --legacy-peer-deps
COPY . .
RUN npm run build --prod

### STAGE 2: Run ###
FROM nginx:latest
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist/carrefour-test/browser /usr/share/nginx/html
EXPOSE 4200

## script.sh script is launched at container run

CMD ["/bin/sh",  "-c",  "exec nginx -g 'daemon off;'"]